#!/sbin/sh
#
# ADDOND_VERSION=3
#
# Addon.d script created from AFZC tool by Nikhil Menghani
#

ps | grep zygote | grep -v grep >/dev/null && BOOTMODE=true || BOOTMODE=false
$BOOTMODE || ps -A 2>/dev/null | grep zygote | grep -v grep >/dev/null && BOOTMODE=true

# [ ! $BOOTMODE ] && [ -z "$2" ] && exit
V1_FUNCS=/tmp/backuptool.functions
V2_FUNCS=/postinstall/system/bin/backuptool_ab.functions
V3_FUNCS=/postinstall/tmp/backuptool.functions
if [ -f $V1_FUNCS ]; then
  . $V1_FUNCS
  backuptool_ab=false
elif [ -f $V2_FUNCS ]; then
  . $V2_FUNCS
elif [ -f $V3_FUNCS ]; then
  . $V3_FUNCS
else
  return 1
fi
if [ -d "/postinstall" ]; then
  P="/postinstall/system"
else
  P="$S"
fi

nikGappsDir="/sdcard/NikGapps"
mkdir -p $nikGappsDir/addonLogs

initialize() {
  ps | grep zygote | grep -v grep >/dev/null && BOOTMODE=true || BOOTMODE=false
  $BOOTMODE || ps -A 2>/dev/null | grep zygote | grep -v grep >/dev/null && BOOTMODE=true
}

initialize

addToLog() {
  mkdir -p "$(dirname /sdcard/NikGapps/addonLogs/addonlogfiles/NikGapps_GBoard_addon.log)";
  echo "$1" >> /sdcard/NikGapps/addonLogs/addonlogfiles/NikGapps_GBoard_addon.log
}

addToLog "Execute Log for GBoard with commands: $@"
# determine parent output fd and ui_print method
FD=1
# update-binary|updater <RECOVERY_API_VERSION> <OUTFD> <ZIPFILE>
OUTFD=$(ps | grep -v 'grep' | grep -oE 'update(.*) 3 [0-9]+' | cut -d" " -f3)
[ -z $OUTFD ] && OUTFD=$(ps -Af | grep -v 'grep' | grep -oE 'update(.*) 3 [0-9]+' | cut -d" " -f3)
# update_engine_sideload --payload=file://<ZIPFILE> --offset=<OFFSET> --headers=<HEADERS> --status_fd=<OUTFD>
[ -z $OUTFD ] && OUTFD=$(ps | grep -v 'grep' | grep -oE 'status_fd=[0-9]+' | cut -d= -f2)
[ -z $OUTFD ] && OUTFD=$(ps -Af | grep -v 'grep' | grep -oE 'status_fd=[0-9]+' | cut -d= -f2)
test "$verbose" -a "$OUTFD" && FD=$OUTFD
if [ -z $OUTFD ]; then
  ui_print() { echo "$1"; test "/sdcard/NikGapps/addonLogs/logfiles/NikGapps.log" && echo "$1" >> "/sdcard/NikGapps/addonLogs/logfiles/NikGapps.log"; }
else
  ui_print() { echo -e "ui_print $1\nui_print" >> /proc/self/fd/$OUTFD; test "/sdcard/NikGapps/addonLogs/logfiles/NikGapps.log" && echo "$1" >> "/sdcard/NikGapps/addonLogs/logfiles/NikGapps.log"; }
fi

if [ -d "/postinstall" ]; then
  P="/postinstall/system"
  T="/postinstall/tmp"
else
  P="$S"
  T="/tmp"
fi

beginswith() {
case $2 in
"$1"*)
  echo true
  ;;
*)
  echo false
  ;;
esac
}

clean_recursive () {
  func_result="$(beginswith / "$1")"
  addToLog "- Deleting $1 with func_result: $func_result"
  if [ "$func_result" = "true" ]; then
    addToLog "- Deleting $1"
    rm -rf "$1"
  else
    addToLog "- Deleting $1"
    # For OTA update
    for sys in "/postinstall" "/postinstall/system"; do
      for subsys in "/system" "/product" "/system_ext"; do
        for folder in "/app" "/priv-app"; do
          delete_recursive "$sys$subsys$folder/$1"
        done
      done
    done
    # For Devices having symlinked product and system_ext partition
    for sys in "$P" "/system" "/system_root"; do
      for subsys in "/system" "/product" "/system_ext"; do
        for folder in "/app" "/priv-app"; do
          delete_recursive "$sys$subsys$folder/$1"
        done
      done
    done
    # For devices having dedicated product and system_ext partitions
    for subsys in "$P" "/system" "/product" "/system_ext"; do
      for folder in "/app" "/priv-app"; do
        delete_recursive "$subsys$folder/$1"
      done
    done
  fi
}

CopyFile() {
  if [ -f "$1" ]; then
    mkdir -p "$(dirname "$2")"
    cp -f "$1" "$2"
  fi
}

delete_recursive() {
  addToLog "- rm -rf $*"
  rm -rf "$*"
}

find_config() {
  nikgapps_config_file_name="$nikGappsDir/nikgapps.config"
  for location in "/tmp" "/sdcard1" "/sdcard1/NikGapps" "/sdcard" "/storage/emulated/NikGapps" "/storage/emulated"; do
    if [ -f "$location/nikgapps.config" ]; then
      nikgapps_config_file_name="$location/nikgapps.config"
      break;
    fi
  done
}

# Read the config file from (Thanks to xXx @xda)
ReadConfigValue() {
  value=$(sed -e '/^[[:blank:]]*#/d;s/[\t\n\r ]//g;/^$/d' "$2" | grep "^$1=" | cut -d'=' -f 2)
  echo "$value"
  return $?
}

find_config

execute_config=$(ReadConfigValue "execute.d" "$nikgapps_config_file_name")
[ "$execute_config" != "0" ] && execute_config=1
addToLog "- execute_config = $execute_config"
addon_version_config=$(ReadConfigValue "addon_version.d" "$nikgapps_config_file_name")
[ -z "$addon_version_config" ] && addon_version_config=3
addToLog "- addon_version_config = $addon_version_config"

list_files() {
cat <<EOF
product/usr/srec/en-US/g2p.data
product/usr/srec/en-US/contacts.abnf
product/usr/srec/en-US/CONTACT_NAME.syms
product/usr/srec/en-US/hmmlist
product/usr/srec/en-US/compile_grammar.config
product/usr/srec/en-US/g2p_phonemes.syms
product/usr/srec/en-US/am_phonemes.syms
product/usr/srec/en-US/endpointer_model.mmap
product/usr/srec/en-US/wordlist.syms
product/usr/srec/en-US/endpointer_voicesearch.config
product/usr/srec/en-US/word_confidence_classifier
product/usr/srec/en-US/dictation.config
product/usr/srec/en-US/phonelist
product/usr/srec/en-US/lexicon.U.fst
product/usr/srec/en-US/offensive_word_normalizer.mfar
product/usr/srec/en-US/endpointer_model
product/usr/srec/en-US/APP_NAME.syms
product/usr/srec/en-US/portable_lstm
product/usr/srec/en-US/TERSE_LSTM_LM.lstm_lm.main_model.uint8.data
product/usr/srec/en-US/TERSE_LSTM_LM.lstm_lm.syms
product/usr/srec/en-US/endpointer_dictation.config
product/usr/srec/en-US/APP_NAME.fst
product/usr/srec/en-US/grammar.config
product/usr/srec/en-US/input_mean_std_dev
product/usr/srec/en-US/config.pumpkin
product/usr/srec/en-US/voice_actions_compiler.config
product/usr/srec/en-US/en-US_read-items_SearchMessageAction-Prompted-Skip_TWIDDLER_FST.fst
product/usr/srec/en-US/g2p_fst
product/usr/srec/en-US/en-US_monastery_contact-disambig-static_TWIDDLER_FST.fst
product/usr/srec/en-US/en-US_confirmation_confirmation-cancellation_TWIDDLER_FST.fst
product/usr/srec/en-US/commands.abnf
product/usr/srec/en-US/en-US_app-actions_prompted-app-name_TWIDDLER_FST.fst
product/usr/srec/en-US/embedded_class_denorm.mfar
product/usr/srec/en-US/magic_mic.config
product/usr/srec/en-US/portable_meanstddev
product/usr/srec/en-US/en-US_read-items_SearchMessageAction-Prompted-Read_TWIDDLER_FST.fst
product/usr/srec/en-US/c_fst
product/usr/srec/en-US/SONG_NAME.syms
product/usr/srec/en-US/CONTACT.transform.mfar
product/usr/srec/en-US/CLG.prewalk.fst
product/usr/srec/en-US/dnn
product/usr/srec/en-US/en-US_monastery_GenericAction-Prompted-ContactName_TWIDDLER_FST.fst
product/usr/srec/en-US/semantics.pumpkin
product/usr/srec/en-US/en-US_media-actions_music-service-controllable_TWIDDLER_FST.fst
product/usr/srec/en-US/CONTACT_NAME.fst
product/usr/srec/en-US/verbalizer_terse.mfar
product/usr/srec/en-US/TERSE_LSTM_LM.lstm_lm.self_normalized_model.uint8.data
product/usr/srec/en-US/prons_exception_dictionary_file.txt
product/usr/srec/en-US/en-US_gmm-actions_gmm-nav-actions_TWIDDLER_FST.fst
product/usr/srec/en-US/embedded_normalizer.mfar
product/usr/srec/en-US/norm_fst
product/usr/srec/en-US/g2p_graphemes.syms
product/usr/srec/en-US/offline_action_data.pb
product/usr/srec/en-US/dict
product/usr/srec/en-US/SONG_NAME.fst
product/usr/srec/en-US/metadata
product/usr/srec/en-US/en-US_time-actions_time-context_TWIDDLER_FST.fst
product/usr/srec/en-US/hmm_symbols
product/usr/srec/en-US/monastery_config.pumpkin
product/usr/srec/en-US/lstm_model.uint8.data
product/usr/srec/en-US/en-US_calendar-actions_AddCalendarEvent-Prompted-FieldToChange_TWIDDLER_FST.fst
product/usr/srec/en-US/rescoring.fst.compact
product/usr/srec/en-US/pumpkin.mmap
product/usr/srec/en-US/voice_actions.config
product/usr/srec/en-US/ep_portable_model.uint8.mmap
product/usr/srec/en-US/ep_portable_mean_stddev
product/lib/libjni_latinimegoogle.so
product/lib64/libjni_latinimegoogle.so
product/app/LatinIMEGooglePrebuilt/LatinIMEGooglePrebuilt.apk
product/usr/share/ime/google/d3_lms/ko_2018030706.zip
product/usr/share/ime/google/d3_lms/zh_CN_2018030706.zip
product/usr/share/ime/google/d3_lms/mozc.data
product/etc/permissions/GBoard.prop
EOF
}

 
if [ "$execute_config" = "0" ]; then
  if [ -f "$S/addon.d/50-GBoard.sh" ]; then
    ui_print "- Deleting up GBoard.sh"
    rm -rf $S/addon.d/50-GBoard.sh
    rm -rf $T/addon.d/50-GBoard.sh
  fi
  exit 1
fi
 
if [ ! -f "$S/addon.d/$fileName.sh" ]; then
  test "$execute_config" = "1" && CopyFile "$0" "$S/addon.d/$fileName.sh"
fi
 
case "$1" in
 pre-backup)
   rm -rf "/sdcard/NikGapps/addonLogs/addonlogfiles/NikGapps_GBoard_addon.log"
 ;;
 backup)
   ui_print "- Backing up GBoard"
   list_files | while read FILE DUMMY; do
     backup_file $S/"$FILE"
   done
 ;;
 post-backup)
   # Stub
 ;;
 pre-restore)
   # Stub
 ;;
 restore)
   ui_print "- Restoring GBoard"
   list_files | while read FILE REPLACEMENT; do
     R=""
     [ -n "$REPLACEMENT" ] && R="$S/$REPLACEMENT"
     [ -f "$C/$S/$FILE" ] && restore_file $S/"$FILE" "$R"
   done
   addToLog "Deleting Aosp apps in GBoard"
   clean_recursive "LatinIME"
   addToLog "Removing Files from Rom Source in GBoard"
   addToLog "Running Debloater in GBoard"
   for i in $(list_files); do
     f=$(get_output_path "$S/$i")
     chown root:root "$f"
     chmod 644 "$f"
     chmod 755 $(dirname $f)
   done
 ;;
 post-restore)
 ;;
esac
